#-------------------------------------------------
#
# Project created by QtCreator 2014-05-02T21:00:19
#
#-------------------------------------------------

CONFIG += static

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = URO
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
