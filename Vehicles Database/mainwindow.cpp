#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtSql>

QSqlQuery *query;
QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
QSqlTableModel *model = new QSqlTableModel();
int aktualni_radek;
QModelIndex akt_radek_data;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    //spojeni s db
    MainWindow::createConnection();

    model->setTable("dbVozidel");
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    model->select();

    ui->setupUi(this);
    ui->comboBox_4->addItem("");
    ui->comboBox_4->addItem("Audi");
    ui->comboBox_4->addItem("BMW");
    ui->comboBox_4->addItem("Ferrari");
    ui->comboBox_4->addItem("Škoda");
    ui->comboBox_2->addItem("");
    ui->comboBox_2->addItem("Sedan");
    ui->comboBox_2->addItem("Nesedan");
    ui->comboBox_3->addItem("");
    ui->comboBox_3->addItem("Benzín");
    ui->comboBox_3->addItem("Nafta");
    ui->comboBox_3->addItem("Plyn");
    if (!QSqlDatabase::drivers().contains("QSQLITE")){
        //vystup do statusbaru
        ui->label_11->setText("Je potřeba mít SQL LITE");
        ui->label_11->setStyleSheet("QLabel { background-color : red; color : black; font : bold}");
    }

    ui->tableView->setModel(model);
    ui->tableView->hideColumn(0);
    ui->tableView->hideColumn(8);
    ui->tableView->hideColumn(9);
    ui->tableView->hideColumn(10);
    ui->tableView->hideColumn(11);
    ui->tableView->hideColumn(12);
    ui->tableView->hideColumn(13);
    ui->tableView->hideColumn(14);
    ui->tableView->hideColumn(15);
    ui->tableView->hideColumn(16);
    ui->tableView->hideColumn(18);
    ui->tableView->hideColumn(19);
    ui->tableView->hideColumn(20);
    ui->tableView->hideColumn(21);
    ui->tableView->hideColumn(22);
}


bool MainWindow::createConnection()
{
    db.setDatabaseName(":memory:");
    if (!db.open()) {
        //vystup do statusbaru
        ui->label_11->setText("Nelze otevřít db spojení, je potřeba podpora SQLite");
        ui->label_11->setStyleSheet("QLabel { background-color : red; color : black; font : bold}");
        return false;
    }

    query = new QSqlQuery(db);

    query->exec("create table dbVozidel (id int primary key, "
               "vyrobce varchar(20), karoserie varchar(20), palivo varchar(20), dvere int, rok int, objem int, vykon int,"
               "abs boolean, airbag boolean, autoalarm boolean, prevodovka boolean, autoradio boolean, central boolean, okno_zadni boolean, okno_predni boolean,"
               "klimatizace boolean, barva varchar(20), posilovac boolean, protiprokluzovy boolean, stresni boolean, tazne boolean, xenony boolean, poznamky varchar(1000))");
    query->exec("insert into dbVozidel values(0, 'Audi', 'Sedan', 'Nafta', '3', '2001', '1900', '70', '1', '0', '0', '1', '0', '0', '0', '1', '0', 'cervena', '0', '1', '0', '0', '1', 'poznamka')");
    query->exec("insert into dbVozidel values(1, 'BMW', 'Nesedan', 'Benzín', '5', '1990', '3000', '100', '0', '1', '1', '0', '0', '0', '0', '1', '0', 'modra', '1', '0', '0', '1', '0', 'poznamka2')");

    return true;
}


MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    //vymazani Zakladni
    ui->comboBox_2->setCurrentIndex(0);
    ui->comboBox_3->setCurrentIndex(0);
    ui->comboBox_4->setCurrentIndex(0);
    ui->spinBox->clear();
    ui->textEdit->clear();
    ui->textEdit_2->clear();
    ui->textEdit_3->clear();

    //vymazat rozsirene
    ui->checkBox->setChecked(false);
    ui->checkBox_2->setChecked(false);
    ui->checkBox_3->setChecked(false);
    ui->lineEdit->clear();
    ui->checkBox_5->setChecked(false);
    ui->checkBox_6->setChecked(false);
    ui->checkBox_7->setChecked(false);
    ui->checkBox_8->setChecked(false);
    ui->checkBox_9->setChecked(false);
    ui->checkBox_10->setChecked(false);
    ui->checkBox_11->setChecked(false);
    ui->checkBox_12->setChecked(false);
    ui->checkBox_13->setChecked(false);
    ui->checkBox_14->setChecked(false);
    ui->checkBox_15->setChecked(false);

    //vymazat poznamky
    ui->textEdit_7->clear();

    //vystup do statusbaru
    ui->label_11->setText("Všechna pole vymazána");
    ui->label_11->setStyleSheet("QLabel { background-color : green; color : black; font : bold}");
}

void MainWindow::on_pushButton_2_clicked()
{
    if(akt_radek_data.isValid()){
        query = new QSqlQuery(db);
        query->prepare("DELETE FROM dbVozidel WHERE id=?");
        query->bindValue(0, akt_radek_data.sibling(aktualni_radek, 0).data());
        query->exec();
        //aktualizace tabulky
        model->select();
        //vystup do statusbaru
        ui->label_11->setText("Záznam na řádku " + QString::number(aktualni_radek) + " byl vymazán");
        ui->label_11->setStyleSheet("QLabel { background-color : green; color : black; font : bold}");
    }
    else{
        //vystup do statusbaru
        ui->label_11->setText("Není vybrán žádný záznam -> nelze nic smazat");
        ui->label_11->setStyleSheet("QLabel { background-color : red; color : black; font : bold}");
    }
}

void MainWindow::on_pushButton_3_clicked()
{

}

void MainWindow::on_pushButton_4_clicked()
{

}

void MainWindow::on_comboBox_4_activated(int index)
{
    //vystup do statusbaru
     ui->label_11->setText("");
     ui->label_11->setStyleSheet("QLabel { background-color : none;}");
}

void MainWindow::on_tableView_clicked(const QModelIndex &index)
{
    aktualni_radek = index.row();
    akt_radek_data = index;
    //vypis cisla vybranyho radku do status baru
    QString name = index.sibling(aktualni_radek, 1).data().toString();
    ui->label_11->setText("Poradi zaznamu: " + QString::number(aktualni_radek) + ", Id: " + index.sibling(aktualni_radek, 0).data().toString() + ", Vyrobce: " + name);
    ui->label_11->setStyleSheet("QLabel { background-color : none;}");

    ui->comboBox_4->setCurrentIndex(ui->comboBox_4->findText(name));
    ui->comboBox_2->setCurrentIndex(ui->comboBox_2->findText(index.sibling(aktualni_radek, 2).data().toString()));
    ui->comboBox_3->setCurrentIndex(ui->comboBox_3->findText(index.sibling(aktualni_radek, 3).data().toString()));
    ui->spinBox->setValue(index.sibling(aktualni_radek, 4).data().toInt());
    ui->textEdit->setText(index.sibling(aktualni_radek, 5).data().toString());
    ui->textEdit_2->setText(index.sibling(aktualni_radek, 6).data().toString());
    ui->textEdit_3->setText(index.sibling(aktualni_radek, 7).data().toString());
    ui->textEdit_7->setText(index.sibling(aktualni_radek, 23).data().toString());
}
