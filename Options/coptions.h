#ifndef COPTIONS_H
#define COPTIONS_H

#include <QtGui/QDialog>
#include <QAbstractButton>
#include <QFileDialog>
#include <QMessageBox>
#include <QDataStream>

namespace Ui
{
    class COptions;
}

class COptions : public QDialog
{
    Q_OBJECT

public:
    COptions(QWidget *parent = 0);
    ~COptions();
    void SaveCfgFile(void);

private:
    Ui::COptions *ui;

private slots:
    void on_pushButton_released();
    void on_buttonBox_clicked(QAbstractButton* button);
};

#endif // COPTIONS_H
