/********************************************************************************
** Form generated from reading ui file 'form.ui'
**
** Created: Sun 6. Sep 09:05:43 2009
**      by: Qt User Interface Compiler version 4.5.2
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

#ifndef UI_FORM_H
#define UI_FORM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QTabWidget>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_COptions
{
public:
    QTabWidget *NewBox;
    QWidget *tab;
    QGroupBox *groupBox;
    QLineEdit *l_PathEdit;
    QPushButton *pushButton;
    QWidget *tab_2;
    QComboBox *comboBox;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *lineEdit;
    QCheckBox *checkBox;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *COptions)
    {
        if (COptions->objectName().isEmpty())
            COptions->setObjectName(QString::fromUtf8("COptions"));
        COptions->resize(600, 294);
        NewBox = new QTabWidget(COptions);
        NewBox->setObjectName(QString::fromUtf8("NewBox"));
        NewBox->setGeometry(QRect(0, 1, 601, 250));
        NewBox->setAutoFillBackground(false);
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        groupBox = new QGroupBox(tab);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(10, 10, 581, 201));
        l_PathEdit = new QLineEdit(groupBox);
        l_PathEdit->setObjectName(QString::fromUtf8("l_PathEdit"));
        l_PathEdit->setEnabled(false);
        l_PathEdit->setGeometry(QRect(30, 90, 455, 20));
        pushButton = new QPushButton(groupBox);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(489, 88, 77, 22));
        NewBox->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        comboBox = new QComboBox(tab_2);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));
        comboBox->setGeometry(QRect(97, 27, 151, 22));
        label = new QLabel(tab_2);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(22, 32, 65, 16));
        label_2 = new QLabel(tab_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(20, 83, 65, 16));
        lineEdit = new QLineEdit(tab_2);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setGeometry(QRect(96, 80, 320, 20));
        checkBox = new QCheckBox(tab_2);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));
        checkBox->setGeometry(QRect(14, 135, 121, 19));
        checkBox->setLayoutDirection(Qt::RightToLeft);
        NewBox->addTab(tab_2, QString());
        buttonBox = new QDialogButtonBox(COptions);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(300, 260, 291, 31));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        buttonBox->setCenterButtons(false);

        retranslateUi(COptions);

        NewBox->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(COptions);
    } // setupUi

    void retranslateUi(QDialog *COptions)
    {
        COptions->setWindowTitle(QApplication::translate("COptions", "Options dialog with TabControl", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("COptions", "Path to file options", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("COptions", "Browse...", 0, QApplication::UnicodeUTF8));
        NewBox->setTabText(NewBox->indexOf(tab), QApplication::translate("COptions", "Path", 0, QApplication::UnicodeUTF8));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("COptions", "Item 1", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("COptions", "Item 2", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("COptions", "Item 3", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("COptions", "Item 4", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("COptions", "Item 5", 0, QApplication::UnicodeUTF8)
        );
        label->setText(QApplication::translate("COptions", "Select Item:", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("COptions", "Type text:", 0, QApplication::UnicodeUTF8));
        checkBox->setText(QApplication::translate("COptions", "CheckBox Item", 0, QApplication::UnicodeUTF8));
        NewBox->setTabText(NewBox->indexOf(tab_2), QApplication::translate("COptions", "Main options", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(COptions);
    } // retranslateUi

};

namespace Ui {
    class COptions: public Ui_COptions {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_H
