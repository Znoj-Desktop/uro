#include "coptions.h"
#include "ui_form.h"

COptions::COptions(QWidget *parent)
    : QDialog(parent), ui(new Ui::COptions)
{
    ui->setupUi(this);
}

COptions::~COptions()
{
    delete ui;
}

void COptions::SaveCfgFile(void)
{
    QFile file(ui->l_PathEdit->text());
    if (!file.open(QFile::WriteOnly))
    {
        QMessageBox::warning(this, tr("FileStream"),
        QString::fromUtf8("Nem��u otev��t soubor %1:\n%2.")
        .arg(ui->l_PathEdit->text())
        .arg(file.errorString()));
        return;
    }
    QDataStream in(&file);
    QApplication::setOverrideCursor(Qt::WaitCursor);
    in << ui->comboBox->currentText();
    in << ui->lineEdit->text();
    in << ui->checkBox->isChecked();
    file.close();
    QApplication::restoreOverrideCursor();
}

void COptions::on_buttonBox_clicked(QAbstractButton* button)
{
    if (button->text() == "OK" || button->text() == "&OK")
    {
        if (ui->l_PathEdit->text().isEmpty())
        {
            QMessageBox::StandardButton reply;
            reply = QMessageBox::critical(this, tr("Error"),"Parametr File save needed.");
        }
        else
        {
            SaveCfgFile();
            QMessageBox::StandardButton reply;
            reply = QMessageBox::information(this, tr("Done"),"Configuration file is saved correct.");
            close();
        }
    }
    else if (button->text() == "Cancel" || button->text() == "&Cancel")
    {
        close();
    }
}

void COptions::on_pushButton_released()
{
    QFileDialog::Options options;
    options |= QFileDialog::DontUseNativeDialog;
    QString selectedFilter;
    QString fileName = QFileDialog::getSaveFileName(this,
                  tr("Select file to save"), "", tr("Text Files (*.cfg)"), &selectedFilter, options);
    if (!fileName.isEmpty())
    {
        if (fileName.indexOf(".cfg") == -1)
            fileName += ".cfg";
        ui->l_PathEdit->setText(fileName);
    }
}
