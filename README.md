### **Description**

GUI in QT - Cars and Employees, Options, Vehicles Database  

---
### **Technology**
C++ (QT)

---
### **Year**
2013

---
### **Screenshots**
#### Cars and employees
![](./README/URO_CarsAndEmployees.png)  
![](./README/CarsAndEmployees1.PNG)  
![](./README/CarsAndEmployees2.PNG)  
![](./README/CarsAndEmployees3.PNG)  
![](./README/CarsAndEmployees4.PNG)  
![](./README/CarsAndEmployees5.PNG)  
![](./README/CarsAndEmployees6.PNG)  
![](./README/CarsAndEmployees7.PNG)  

#### Vehicles Database
Without screenshot  

#### Options
Without screenshot  