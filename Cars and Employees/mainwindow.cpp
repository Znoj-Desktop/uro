#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->comboBox->addItem("");
    ui->comboBox->addItem("Nafta");
    ui->comboBox->addItem("Benzín");
    ui->comboBox->addItem("Plyn");

    ui->comboBox_2->addItem("Letní");
    ui->comboBox_2->addItem("Zimní");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionKonec_triggered()
{
    QApplication::quit();
}

void MainWindow::on_actionO_programu_triggered()
{
    QWidget *popup = new QWidget();
    popup->resize(350, 100);
    popup->setWindowTitle("Jakub Říha, URO 2014");
    popup->show();
}
